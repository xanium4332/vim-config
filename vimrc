call plug#begin('~/.vim/plugged')

" ---- Plugins ----

" Sensible defaults
Plug 'tpope/vim-sensible'

" ctrl+p
Plug 'ctrlpvim/ctrlp.vim'

" Powerline
Plug 'vim-airline/vim-airline'

" NERDtree with on-demand loading
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
map <C-n> :NERDTreeToggle<CR>

" Quick switching between headers and source files ( :A )
Plug 'vim-scripts/a.vim'

" For background running of tasks (in conventional Vim)
Plug 'tpope/vim-dispatch'

" Syntax checking and making (neovim alternative = neomake)
Plug 'scrooloose/syntastic'

" Autocomplete (neovim alternative = deoplete)
" Plug 'Valloric/YouCompleteMe'
" Lightweight alternative
Plug 'ervandew/supertab'

" Snippets that work with the above autocompletion
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets' " Snippets are separate from the engine

" ---- Language support ----

" CSV
Plug 'chrisbra/csv.vim'

" Rust
Plug 'rust-lang/rust.vim'

" Protocol buffers
Plug 'uarun/vim-protobuf'

" ---- Colorschemes ----
Plug 'crusoexia/vim-monokai'
Plug 'roosta/srcery'
Plug 'nanotech/jellybeans.vim'

" ---- Configuration ----

" Split windows below and right instead of above and left
set splitbelow splitright

" Search settings
set hlsearch   " Highlight searches
set incsearch  " Highlight dynamically as pattern is typed
set ignorecase " Make searches case-insensitive...
set smartcase  " ...unless they contain at least one uppercase character
set gdefault   " Use global search by default
nnoremap <silent> <Leader>/ :nohlsearch<CR> " Clear search highlighting

" Force 256 colours
set t_co=256

call plug#end()

" Colorscheme
colorscheme jellybeans

