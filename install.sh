#!/bin/sh

# Must be run inside directory containing this file

curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
ln -s $PWD/vimrc ~/.vimrc
